package org.example

fun main() {
    val avarage = readLine()!!.toDouble();

    when {
        avarage < 5 -> println("REP");
        avarage >= 5 && avarage < 7 -> println("REC");
        else -> println("APR");
    }
}