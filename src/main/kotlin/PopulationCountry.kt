package org.example

data class Country(var population: Double, val rate: Double) {
    fun growPopulationAnnual(): Double {
        return (rate * population) / 100
    }
}

fun calculateYear (inhabitantsCountryA: Double, inhabitantsCountryB: Double): Int {
    val countryA = Country(inhabitantsCountryA, rate = 3.0)
    val countryB = Country(inhabitantsCountryB, rate = 1.5)
    var quantityYears = 0

    while (countryA.population <= countryB.population) {
        countryA.population += countryA.growPopulationAnnual()
        countryB.population += countryB.growPopulationAnnual()
        quantityYears++
    }

    return  quantityYears
}

fun main() {
    try {
        val inhabitantsCountryA = readLine()!!.toDouble();
        val inhabitantsCountryB = readLine()!!.toDouble();

        if (inhabitantsCountryA > inhabitantsCountryB) {
            throw Exception("Número de habitantes A é maior que o número habitantes B")
        }

        val result = calculateYear(inhabitantsCountryA, inhabitantsCountryB);

        println("$result anos")
    } catch (e: Throwable) {
        println(e.message)
    }
}