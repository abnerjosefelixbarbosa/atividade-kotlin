package org.example

object ReceitaFederal {
    fun calculateTax(salary: Double): Double {
        val aliquot = when {
            (salary >= 0 && salary <= 1100) -> 0.05
            (salary >= 1100.01 && salary <= 2500) -> 0.10
            else -> 0.15
        }
        return aliquot * salary
    }
}

fun calculateBenefit(valueSalary: Double, valueBenefits: Double): Double {
    val valueTax = ReceitaFederal.calculateTax(valueSalary);
    return (valueSalary - valueTax) + valueBenefits;
}

fun main() {
    try {
        val valueSalary = readLine()!!.toDouble();
        val valueBenefits = readLine()!!.toDouble();
        val value = calculateBenefit(valueSalary, valueBenefits);
        println(String.format("%.2f", value));
    } catch (e: Throwable) {
        println(e.message)
    }
}