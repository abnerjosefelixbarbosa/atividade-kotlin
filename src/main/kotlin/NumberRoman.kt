package org.example

fun convertNumberRomanToNumber (numberRoman: String): Int {
    try {
        val numbersRomans = mapOf(
            'I' to 1,
            'V' to 5,
            'X' to 10,
            'L' to 50,
            'C' to 100,
            'D' to 500,
            'M' to 1000
        )
        var result = 0

        for (i in numberRoman.indices) {
            val currentNumber = numbersRomans.getValue(numberRoman[i])
            val nextNumber = when (i + 1) {
                numberRoman.length -> 0
                else -> numbersRomans.getValue(numberRoman[i + 1])
            }

            if (currentNumber < nextNumber) {
                result -= currentNumber
            } else {
                result += currentNumber
            }
        }

        return  result
    } catch (e: Exception) {
        e.printStackTrace()
        return 0
    }
}

fun main() {
    val numberRoman: String = readLine().toString()
    val number = convertNumberRomanToNumber(numberRoman)

    println(number)
}